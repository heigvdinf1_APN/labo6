#include <string>
#include <iostream>
#include <cmath>

using namespace std;

/**
 \brief Traduit des nombres réels en prix exprimés en vaudois
 \param[in] montant un réel compris entre 0 et 999999.99 CHF.
 \return une chaine de caractères indiquant en vaudois le prix
 en francs et centimes.
 \details Exemples:
 12.30  -> "douze francs et trente centimes"
 200.01 -> "deux cents francs et un centime"
 180    -> "cent huitante francs"
 1.80   -> "un franc et huitante centimes"
 0.20   -> "vingt centimes"
 0      -> "zéro franc"
 */
string montantEnVaudois(double montant);

/**
 \brief Traduit des nombres réels en nombre exprimés en vaudois
 \param[in] nombre un entier compris entre 0 et 999.
 \return une chaine de caractères indiquant en vaudois le chiffre

 */
string affichelacentaine(int nombre);
string milliers(int montant);
string centaines(int montant);

/**
 * @brief Traduit un nombre entier inférieur à 100 en son équivalent vaudois en tout lettre.
 * @param montant est un nombre entier entre 1 et 99.
 * @return une chaine de caractère contentant le nombre en toute lettre.
 */
string dizaines(int montant);

/**
 * @brief Traduit un nombre entier inférieur à 10 en son équivalent vaudois en tout lettre.
 * @param montant est un nombre entier entre 1 et 9.
 * @return une chaine de caractère contentant le nombre en toute lettre.
 */
string unites(int montant);

int main()
{
    double d;

    while( cin >> d ) {
        cout << montantEnVaudois(d) << endl;
    }

    return 0;
}



string milliers(int montant){
    string millier;

    if (montant >= 1){
        if(montant > 1){
            millier = centaines(montant);
        }
        millier += "mille";
    }

    return millier;
}

string centaines(int montant){
    int nbCentaine = montant /100;
    string centaine;
    if(nbCentaine != 1){
        centaine = unites(nbCentaine);
    }
    if(montant % 100 == 0){
        centaine += "cents";
    }else{
        centaine += "cent ";
        centaine += dizaines(montant % 100);
    }
    return centaine;
}

string dizaines(int montant){
    string dizaine ="";
    switch(montant-montant%10){
        case 10 :
                switch(montant){
            case 11 : dizaine = "onze";
                break;
            case 12 : dizaine = "douze";
                break;
            case 13 : dizaine = "treize";
                break;
            case 14 : dizaine = "quatorze";
                break;
            case 15 : dizaine = "quinze";
                break;
            case 16 : dizaine = "seize";
                break;
            default : dizaine = "dix";
                break;
            }

            break;

        case 20 : dizaine = "vingt";
            break;
        case 30 : dizaine = "trente";
           break;
        case 40 : dizaine = "quarante";
           break;
        case 50 : dizaine = "cinquante";
           break;
        case 60 : dizaine = "soixante";
           break;
        case 70 : dizaine = "septante";
           break;
        case 80 : dizaine = "huitante";
           break;
        case 90 : dizaine = "nonante";
           break;
       }

if((montant >16)and(montant%10 != 0)){
    if(montant%10 == 1){
        dizaine += " et ";
    }
    else{
        dizaine +="-";
    }
    dizaine += unites(montant%10);
}
else if (montant < 10){
    dizaine += unites(montant);
}
    return dizaine;
}

string unites(int montant){
    string unite = "";
    switch(montant){
    case 1 : unite = "un";
            break;
    case 2 : unite = "deux";
            break;
    case 3 : unite = "trois";
            break;
    case 4 : unite = "quatre";
            break;
    case 5 : unite = "cinq";
            break;
    case 6 : unite = "six";
            break;
    case 7 : unite = "sept";
            break;
    case 8 : unite = "huit";
            break;
    case 9 : unite = "neuf";
            break;
    default : unite = "";
            break;
    }
    return unite+" ";
}
/*
string affichelacentaine(int nombre, bool pluriel = 1) {
    string resultat = "";
    int tmp = 0;
    int i = log(nombre) / log(10);
    while (i >= 0) {
        if (i == 2) {
            tmp = nombre / 100;
            if (tmp != 1)
                resultat += dizaines(nombre / 100) + " ";
            resultat += "cent";
            if (nombre > 199 and nombre % 10 == 0 and !pluriel) {
                resultat += "s";
            }
            resultat += " ";
            --i;
            continue;
        }
        resultat += dizaines(nombre % 100);
        break;
    }
    return resultat;
}

string montantEnVaudois(double montant) {
    string resultat;
    int tmp = 0;
    int tmp2 = 0;
    const int BASE = 10;
    const int NBRITERATION = log(montant) / log(BASE);
    for (int i = (NBRITERATION / 3); i >= 0; --i) {
        tmp2 = montant / pow(BASE, (i + 1)*3);
        tmp = montant / pow(BASE, i * 3) - tmp2 * pow(BASE, 3);
        resultat += affichelacentaine(tmp, i);
        switch (i) {
            case 1:
                resultat += "mille";
                continue;
            case 0:
                resultat += " franc";
                if (tmp > 1) {
                    resultat += "s";
                }
                resultat += " ";
                break;
        }
        tmp = (int) round((montant - int(montant)) * pow(BASE, 2)) % (int) pow(BASE, 2); //calcul des centimes
        if (tmp) {
            resultat += " et "+ dizaines(tmp) + " centime";
            if (tmp > 1) {
                resultat += "s";
            }
        }
        return resultat;
    }
}
*/
string montantEnVaudois(double montant) {
    string resultat;
    const int BASE = 10;
    const string unite[2] = {"franc", "mille"}; //les itération suivante serait million, milliard,...
    const string espace = " ", pluriel = "s", centaine = "cent",
            liaison = "et", zero = "zero", petiteUnite = "centime";
    int tmp = 0;
    double reste = montant;
    if (montant == 0) { //on effectue ce test ici car le log(0) ne peut pas être effectuer
        return zero + espace + unite[0];
    }
    //le nombre peut se réecrire sous forme a*10^5+b*10^4+...+g*10^-2, i prend les valeurs
    // des exposant succesive de la base 10
    for (int i = log(montant) / log(BASE); i >= -2; --i) {
        /* l'exposant peut s'ecrire sous la forme d'un modulo de 3 (i=a+3*k)
        lorsque i est negatif il s'agit des centimes nous traiterons ce cas a part
        pour garder de la visibilite dans notre code */
        int modulotrois = (i > 0) ? i % 3 : i;
        switch (modulotrois) {
                //tout les cas où i=2+k*3 ou k est un entier est une centaine
            case 2:
                tmp = reste / pow(BASE, i);
                reste -= tmp * pow(BASE, i);
                if (tmp == 0)
                    break;
                if (tmp > 1) {
                    resultat += unites(tmp) + espace;
                }
                resultat += centaine;
                if (i == 2 and int(reste) == 0 and tmp > 1) {//regle du pluriel de cent
                    resultat += pluriel;
                }
                if (tmp or i == 2) { //si on affiche cent on ajoute un espace
                    resultat += espace;
                }
                break;
            case 1://tout les cas ou i=1+3*k sont des dizaine nous les couplons donc avec les unite d'ou le break
            case -1://en négatif nous sommes dans les centimes, les dizaine de centime sont pris avec les unite des centimes
                break;
                //tout les cas ou  i=3*k sont des unite, nous les couplons donc avec les dizaines suppérieur
            case 0:
                tmp = reste / pow(BASE, i);
                reste -= tmp * pow(BASE, i);
                resultat += dizaines(tmp);
                if (tmp)
                    resultat += espace;
                if (i == 3) {
                    resultat += unite[i / 3] + espace;
                } else if (i == 0 and montant >= 1) {
                    resultat += unite[i];
                    if (montant >= 2) { //regle du pluriel de franc
                        resultat += pluriel;
                    }
                }
                break;
            case -2: //le cas -2 est le cas particulier des centimes
                tmp = round(reste * pow(BASE, 2)); //calcul des centimes
                if (tmp) {
                    if (montant > 1) {
                        resultat += espace + liaison + espace;
                    }
                    resultat += dizaines(tmp) + espace + petiteUnite;
                    if (tmp > 1) { //regle du pluriel de centime
                        resultat += pluriel;
                    }
                }

        }
    }
    return resultat;
}